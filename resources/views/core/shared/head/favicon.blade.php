{{-- Apple Save to Home Screen --}}
<link rel=apple-touch-icon sizes=180x180 href="{{ secure_asset('img/favicons/apple-touch-icon.png') }}">

{{-- General Browser Favicons --}}
<link rel=icon type=image/png sizes=32x32 href="{{ secure_asset('img/favicons/favicon-32x32.png') }}">
<link rel=icon type=image/png sizes=16z16 href="{{ secure_asset('img/favicons/favicon-16x16.png') }}">

{{-- Android Website Manifest --}}
<link rel=manifest href="{{ secure_asset('img/favicons/site.webmanifest') }}">

{{-- Safari Pinned Tab --}}
<link rel=mask-icon type=image/png href="{{ secure_asset('img/favicons/safari-pinned-tab.svg') }}">

{{-- Windows Phone Tile --}}
<meta name=msapplication-TileColor content=#005cc5>
<meta name=theme-color content=#ffffff>