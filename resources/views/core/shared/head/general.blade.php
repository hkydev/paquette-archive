{{-- Control the behavior of search engine crawling and indexing --}}
<meta name=robots content="index,follow"> {{-- All Search Engines --}}
<meta name=googlebot content="index,follow"> {{-- Google Specific --}}

{{-- Tells Google not to show the sitelinks search box --}}
<meta name=google content=nositelinkssearchbox>

{{--Tells Google not to provide a translation for this page--}}
<meta name=google content=notranslate>
