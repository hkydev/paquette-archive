import React from 'react'
import { CardSection } from '@paquette/ui/react/cards'
import { Container } from '@paquette/ui/react/containers'
import Stripe from '../providers/stripe/StripeLogo'
import Paypal from '../providers/paypal/PaypalLogo'
import Square from '../providers/square/SquareLogo'

export const create = () => (
  <div className="amethyst-connections-create">
    <Container>
      <h1>Add Connection</h1>
      <div className="amethyst-connections-create-grid">
        <a href="/amethyst/connect/stripe" className="card">
          <CardSection className="amethyst-connections-create-provider-card-section">
            <Stripe className="amethyst-connections-create-provider-logo" />
          </CardSection>
        </a>
        <a href="/amethyst/connect/paypal" className="card">
          <CardSection className="amethyst-connections-create-provider-card-section">
            <Paypal className="amethyst-connections-create-provider-logo" />
          </CardSection>
        </a>
        <a href="/amethyst/connect/square" className="card">
          <CardSection className="amethyst-connections-create-provider-card-section">
            <Square className="amethyst-connections-create-provider-logo" />
          </CardSection>
        </a>
      </div>
    </Container>
  </div>
)

export default create
