import React from 'react'
import { Action } from '@paquette/ui/react/actions'
import {
  Dropdown,
  DropdownMenu,
  DropdownMenuItem,
} from '@paquette/ui/react/dropdowns'
import { Query } from 'react-apollo'
import gql from 'graphql-tag'
import { Link } from 'react-router-dom'

const FETCH_CURRENT_USER = gql`
  query FETCH_CURRENT_USER {
    currentUser {
      id
      first_name
    }
  }
`

const token: HTMLMetaElement = document.head!.querySelector(
  'meta[name="csrfToken"]'
) as HTMLMetaElement

export const GlobalAccount = () => {
  return (
    <Query query={FETCH_CURRENT_USER}>
      {({ loading, error, data }) => {
        if (loading) return 'loading...'
        if (error) return 'error :('

        const { first_name, id } = data.currentUser

        return (
          <Dropdown
            toggle={<Action>{first_name}</Action>}
            menu={
              <DropdownMenu>
                <DropdownMenuItem>
                  <Link to={`/profile/${id}`}>Profile</Link>
                </DropdownMenuItem>
                <hr />
                <form action="/logout" method="POST">
                  <input type="hidden" name="_token" value={token.content} />
                  <button type="submit">Logout</button>
                </form>
              </DropdownMenu>
            }
          />
        )
      }}
    </Query>
  )
}

export default GlobalAccount
