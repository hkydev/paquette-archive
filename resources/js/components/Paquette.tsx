import React from 'react'
import { BrowserRouter as Router, Switch } from 'react-router-dom'
import { ApolloProvider } from 'react-apollo'
import client from '../api/client'
import GlobalTopbar from './global/GlobalTopbar'
import { AmethystRoutes, CoreRoutes } from '../routes'

export const Paquette = () => (
  <Router>
    <>
      <ApolloProvider client={client}>
        <GlobalTopbar />
        <main id="paquette-view">
          <Switch>
            <AmethystRoutes />
            <CoreRoutes />
          </Switch>
        </main>
      </ApolloProvider>
    </>
  </Router>
)

export default Paquette
