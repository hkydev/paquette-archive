import React, { Component } from 'react'
import { Action, ActionGroup } from '@paquette/ui/react/actions'
import { Btn as Button } from '@paquette/ui/react/buttons'
import { Card, CardSection } from '@paquette/ui/react/cards'
import { Container } from '@paquette/ui/react/containers'
import {
  FormActions,
  FormAlert,
  FormControl,
  FormGroup,
  FormLabel,
} from '@paquette/ui/react/forms'
import { PaquetteWordmark as Wordmark } from '@paquette/ui/react/branding'

interface ReactComponentLoginProps {}
interface ReactComponentLoginState {
  csrf_token: string
  errors: any
}

export default class Login extends Component<
  ReactComponentLoginProps,
  ReactComponentLoginState
> {
  constructor(props: ReactComponentLoginProps) {
    super(props)

    this.state = {
      csrf_token: '',
      errors: [],
    }
  }

  componentDidMount = () => {
    const token: HTMLMetaElement = document.head!.querySelector(
      'meta[name="csrfToken"]'
    ) as HTMLMetaElement

    const authenticationErrors: HTMLMetaElement = document.head!.querySelector(
      'meta[name="authenticationErrors"]'
    ) as HTMLMetaElement

    const errors = JSON.parse(authenticationErrors.content.replace(/'/g, '"'))

    this.setState({
      csrf_token: token.content as string,
      errors,
    })
  }

  render(): JSX.Element {
    const { csrf_token, errors } = this.state

    return (
      <Container>
        <form method="POST" action="/login">
          <a href="/">
            <Wordmark />
          </a>
          <Card className="login-form">
            <CardSection>
              <input type="hidden" name="_token" value={csrf_token} />
              <FormGroup>
                <FormLabel id="email" text="Email" />
                <FormControl id="email" placeholder="name@domain.com" />
                {typeof errors.email !== 'undefined' && errors.email.length ? (
                  <FormAlert
                    text={errors.email[0]}
                    modifiers={{ color: 'error' }}
                  />
                ) : (
                  undefined
                )}
              </FormGroup>
              <FormGroup>
                <FormLabel id="password" text="Password" />
                <FormControl
                  id="password"
                  type="password"
                  placeholder="••••••••••••"
                />
                {typeof errors.password !== 'undefined' &&
                errors.password.length ? (
                  <FormAlert
                    text={errors.password[0]}
                    modifiers={{ color: 'error' }}
                  />
                ) : (
                  undefined
                )}
              </FormGroup>
              <FormActions>
                <Button modifiers={{ color: 'primary', size: 'large' }}>
                  Login
                </Button>
              </FormActions>
            </CardSection>
          </Card>
          <ActionGroup className="links">
            <Action tag="a" href="/password/reset">
              Forgot your Password?
            </Action>
            <Action tag="a" href="/register">
              Register
            </Action>
          </ActionGroup>
        </form>
      </Container>
    )
  }
}
