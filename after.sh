#!/bin/sh

# If you would like to do some extra provisioning you may
# add any commands you wish to this file and they will
# be run after the Homestead machine is provisioned.

# run apt-get update first, without it I was getting errors not finding the extensions
sudo DEBIAN_FRONTEND=noninteractive apt-get --assume-yes update

# load any extensions you like here
sudo DEBIAN_FRONTEND=noninteractive apt-get --assume-yes install php-xdebug
sudo DEBIAN_FRONTEND=noninteractive apt-get --assume-yes install php7.2-bcmath