const mix = require('laravel-mix')
const { resolve } = require('path')

mix
  /**
   * Set some custom configurations
   */
  .webpackConfig({
    resolve: {
      alias: {
        components: resolve(__dirname, 'resources/js/components'),
      },
      symlinks: false,
    },
  })

  /**
   * Compile Assets
   */
  .ts('resources/js/app.tsx', 'public/js')
  .extract([
    '@paquette/ui',
    'apollo-boost',
    'apollo-client',
    // 'echarts',
    // 'echarts-for-react',
    // 'graphql',
    'react',
    // 'react-apollo',
    'react-dom',
    'react-router-dom',
  ])
  .sass('resources/scss/app.scss', 'public/css', {
    outputStyle: 'compressed',
  })
  .sass('resources/scss/auth.scss', 'public/css', {
    outputStyle: 'compressed',
  })
  .options({
    autoprefixer: {
      options: {
        autoprefixer: {
          browsers: [
            '> 5%',
            'last 2 firefox versions',
            'last 2 chrome versions',
            'last 2 safari versions',
            'last 2 edge versions',
            'ie 11',
          ],
          grid: true,
        },
      },
    },
  })

  /**
   * Copy Image files over from assets
   */
  .copyDirectory('resources/img', 'public/img')
  .copy('resources/img/favicons/favicon.ico', 'public')

/**
 * Version the files, but only for production deployment
 */
if (mix.inProduction()) {
  mix.version()
} else {
  mix.sourceMaps()
}
