<?php

return [
    'appsalt' => env('APP_HASH_SALT', 'paquette'),
    'minSlugLength' => env('APP_HASH_MINLENGTH', 16),
    'alphabet' => env('APP_HASH_ALPHABET', '0123456789cfhistuCFHISTU'),
];
