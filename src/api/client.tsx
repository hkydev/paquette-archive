import ApolloClient from 'apollo-boost'

// Authentication Details for Apollo Queries
const token: HTMLMetaElement = document.head!.querySelector(
  'meta[name="token"]'
) as HTMLMetaElement

const client = new ApolloClient({
  headers: {
    'X-CSRF-TOKEN': token ? token.content : '',
    authorization: token ? token.content : '',
  },
  uri: '/graphql',
})

export default client
