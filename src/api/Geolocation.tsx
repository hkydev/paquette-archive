import axios from 'axios'

export default class Geolocation {
  constructor(
    protected prefix: string = 'geolocation:',
    protected url: string = 'https://ipapi.co/json',
    protected fields: string[] = [
      'ip',
      'city',
      'region',
      'region_code',
      'country',
      'country_name',
      'continent_code',
      'in_eu',
      'postal',
      'latitude',
      'longitude',
      'timezone',
      'utc_offset',
      'country_calling_code',
      'currency',
      'languages',
      'asn',
      'org',
    ]
  ) {
    this.fields = fields
    this.prefix = prefix
    this.url = url
  }

  /**
   * Consider what needs to be done when loaded
   * @returns void
   */
  public handle() {
    // Check for any missing key value pairs
    const check = this.checkEntries(this.fields)

    // If we're missing anything expected, remove existing ones and refetch
    if (check.length) {
      this.clearEntries(this.fields)
      this.createEntries()
    }
  }

  /**
   * Check that all of the entires we expect have a value, otherwise return the ones missing
   * @param fieldKeys string[]
   * @returns string[]
   */
  private checkEntries(fieldKeys: string[]): string[] {
    return fieldKeys.filter(key => {
      const value = localStorage.getItem(this.prefix + key)
      return value ? undefined : key
    })
  }

  /**
   * Remove all of the entries
   * @param fieldKeys string[]
   */
  private clearEntries(fieldKeys: string[]) {
    fieldKeys.forEach(key => {
      localStorage.removeItem(key)
    })
  }

  /**
   * Fetch and store the geolocation data to localStorage
   * @returns void
   */
  private createEntries() {
    axios.get(this.url).then(response => {
      const { data } = response
      Object.keys(data).forEach((key: string) => {
        localStorage.setItem(this.prefix + key, data[key])
      })
    })
  }
}
