<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoreUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('core_users', function (Blueprint $table) {
            /**
             * Identifiers
             */
            $table->unsignedBigInteger('id')->unique()->nullable();
            $table->increments('increment_id');

            /**
             * Fields
             */
            $table->string('base_currency')->nullable();
            $table->string('email')->unique();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('locale')->nullable();
            $table->string('password');
            $table->json('preferences')->nullable();
            $table->boolean('staff')->default(false);
            $table->string('timezone')->nullable();
            $table->string('type')->default('standard');
            $table->rememberToken();

            /**
             * Cashier Fields
             */
            $table->string('stripe_id')->nullable();
            $table->string('card_brand')->nullable();
            $table->string('card_last_four')->nullable();
            $table->timestamp('trial_ends_at')->nullable();

            /**
             * Timestamps
             */
            $table->timestamp('email_verified_at')->nullable();
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('core_users');
    }
}
