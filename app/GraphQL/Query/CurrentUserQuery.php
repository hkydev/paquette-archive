<?php

namespace Paquette\GraphQL\Query;

use Paquette\User;
use Auth;
use GraphQL;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class CurrentUserQuery extends Query
{
    protected $attributes = [
        'name' => 'CurrentUserQuery',
        'description' => 'A query',
    ];

    public function type()
    {
        return GraphQL::type('User');
    }

    public function args()
    {
        return [];
    }

    public function resolve($root, $args, SelectFields $fields, ResolveInfo $info)
    {
        return Auth::user();
    }
}
