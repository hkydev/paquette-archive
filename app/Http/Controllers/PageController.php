<?php

namespace Paquette\Http\Controllers;

class PageController extends Controller
{
    public function app()
    {
        return view('base');
    }
}
