<?php

namespace Paquette\Providers;

use Illuminate\Support\Facades\URL;
use Illuminate\Support\ServiceProvider;
use Laravel\Passport\Passport;
use Paquette\Observers\UserObserver;
use Paquette\User;
use Stripe\Stripe;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        /**
         * Enforce the application to be served over HTTPS
         */
        URL::forceScheme('https');

        /**
         * Trigger a User Model Observer
         */
        User::observe(UserObserver::class);

        /**
         * Configure the Passport Session Cookie
         */
        Passport::cookie(config('session.cookie') . '_token');

        /**
         * Set Stripe API Key to be used by Amethyst
         */
        $stripe_env = env('STRIPE_MODE', 'test');
        Stripe::setApiKey(config("amethyst.stripe.$stripe_env.secret_key"));
        Stripe::setApiVersion(env('STRIPE_VERSION'));
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
