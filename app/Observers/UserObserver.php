<?php

namespace Paquette\Observers;

use Paquette\User;

class UserObserver
{
    /**
     * Listen to the User created event.
     *
     * @param  \Paquette\User $user
     *
     * @return void
     */
    public function created(User $user)
    {
        $user->id = $user->slug();
        $user->save();
    }
}
